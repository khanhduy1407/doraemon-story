<p align="center">
    <img src="./banner.jpg" alt="banner" />
</p>

# Doraemon Story

Japan's adored Doraemon meets Story of Seasons in this new, fresh take on farming. The setting is Natura, and the theme is creating bonds. While doing so, enjoy the heartwarming interactions through each character in the story!

## About this game

Doraemon meets Story of Seasons in this new, fresh take on farming!

The setting is Natura, and at the center of this land is the mystical Big Tree. Doraemon and friends will each take on a role to help out around town!

The theme of this experience is creating bonds with the town residents, and while doing so, enjoying the heart-warming interactions through each character and the part they play in the story.

But building a farm and raising horses and cattle like in other Story of Seasons games isn't the only thing to do. Explore the town of Natura, go on adventures, catch bugs and much much more! Fans of the Doraemon series will also be happy to know that Doraemon's gadgets can be used to assist you in your new, everyday life!

Enjoy the nostalgia of Doraemon and Story of Seasons while helping Noby live his farm life in this unique, lovable land!

## System Requirements

### Minimum

- Requires a 64-bit processor and operating system
- OS: Windows 7 SP1 64-bit or Windows 10 64-bit
- Processor: Intel Core2 Duo E8400 or AMD Phenom II X2 550
- Memory: 4 GB RAM
- Graphics: GeForce 9800 GTX+ or Radeon HD 3870
- DirectX: Version 11
- Storage: 750 MB available space
- Sound Card: DirectX compatible soundcard or onboard chipset

### Recommended

- Requires a 64-bit processor and operating system
